# Installation

## Install Packages
  npm install

## Install MySQL migrations
*execute MySQL commands found inside `migrations.sql` file*

## Run server
  nodemon server.js
  http://localhost:3000

*API actions can be found inside `server.js` file*

## Tests
  mocha
