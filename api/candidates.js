var express = require('express')
var router = express.Router()
var pool = require('../database');

router.get('/candidates', function(req, res) {
  pool.getConnection(function(err, connection) {
    connection.query('SELECT * FROM candidates', [], function (error, results, fields) {
      if (error) { // print the error in the console with http code 400
        console.error('error', error)
        res.status(400)
        res.send('Bad Request')
      }
      res.send(results)
    });
  });
})

module.exports = router
