var express = require('express')
var router = express.Router()
var pool = require('../database');
var _ = require('underscore')

 // print the error in the console with http code 400
function handleError(output, err) {
  console.error('error', err)
  output.status(400)
  output.send('Bad Request')
}

// return a 403 result because max votes reached
function votesLimit(output) {
  output.status(403)
  output.send('Forbidden')
}

// validates that poll, candidate and voter exist in the database 
function checkIfPollCandidateAndVoterExist(con, output, pollId, candidateId, voterId) {
  var exists = true
  
  return con.query('SELECT * FROM polls WHERE ?', {id:pollId}, function (pollError, pollResults) { // check if poll exists
    if (pollError)
      handleError(output, pollError)
    if (_.size(pollResults) == 0)
      exists = false // invalid pollId

    return con.query('SELECT * FROM candidates WHERE ?', {id:candidateId}, function (canError, canResults) { // check if candidate exists
        if (canError)
          handleError(output, canError)
        if (_.size(canResults) == 0)
          exists = false // invalid candidateId

        return con.query('SELECT * FROM voters WHERE ?', {id:voterId}, function (voterError, voterResults) { // check if voter exists
          if (voterError)
            handleError(output, voterError)
          if (_.size(voterResults) == 0)
            exists = false // invalid voterId

          return exists
        })
      })
    })
}

router.post('/vote/:pollId/:candidateId/:voterId', function(req, output) {
  pool.getConnection(function(err, con) {
    var {pollId, candidateId, voterId} = req.params
    if (checkIfPollCandidateAndVoterExist(con, output, pollId, candidateId, voterId)) { // check if params are valid entries in the database
      con.query('SELECT count(id) as votes FROM votes WHERE ?', {voter_id:voterId}, function (error, results) {
        if (error)
          handleError(output, error)
        else { // continue the vote process
          if (results[0].votes > 2) // reached allowed votes limit
            votesLimit(output)
          else { // can vote
            var newVote = [[parseInt(voterId), parseInt(candidateId), parseInt(pollId)]]

            con.query('INSERT INTO votes (voter_id, candidate_id, poll_id, voted_at) VALUES (?, NOW())', newVote, function (newVoteError, newVoteRes) {
              if (newVoteError) // handle error
                handleError(output, newVoteError)
              else { // vote ok
                output.status(201)
                output.send(newVoteRes)
              }
            })
          }
        }
      });
    }
    else { // poll / candidate / voter doesn't exist
      output.status(403)
      output.send('Forbidden 1')
    }
  });
})

module.exports = router
