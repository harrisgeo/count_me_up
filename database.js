var mysql = require('mysql')

var pool = mysql.createPool({
  host: 'localhost',
  user: 'bbc_cmu',
  password: 'qwerty12345',
  database: 'count_me_up'
})

exports.getConnection = function(callback) {
  pool.getConnection(function(err, conn) {
    if(err) {
      return callback(err);
    }
    callback(err, conn);
  });
};
