CREATE DATABASE count_me_up;
CREATE USER 'bbc_cmu'@'localhost' IDENTIFIED BY 'qwerty12345';
CREATE DATABASE count_me_up;
GRANT ALL PRIVILEGES ON count_me_up.* TO 'bbc_cmu'@'localhost' WITH GRANT OPTION;


CREATE TABLE IF NOT EXISTS `polls` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `polls`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
INSERT INTO `count_me_up`.`polls` (`name`) VALUES ('count me up');


CREATE TABLE IF NOT EXISTS `candidates` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `candidates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
INSERT INTO `count_me_up`.`candidates` (`name`) VALUES ('A');
INSERT INTO `count_me_up`.`candidates` (`name`) VALUES ('B');
INSERT INTO `count_me_up`.`candidates` (`name`) VALUES ('C');
INSERT INTO `count_me_up`.`candidates` (`name`) VALUES ('D');


CREATE TABLE IF NOT EXISTS `voters` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `voters`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `voters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
INSERT INTO `count_me_up`.`voters` (`name`) VALUES ('John Smith');
INSERT INTO `count_me_up`.`voters` (`name`) VALUES ('Jack Jones');
INSERT INTO `count_me_up`.`voters` (`name`) VALUES ('Harry Williams');
INSERT INTO `count_me_up`.`voters` (`name`) VALUES ('George Taylor');
INSERT INTO `count_me_up`.`voters` (`name`) VALUES ('Jacob Davies');
INSERT INTO `count_me_up`.`voters` (`name`) VALUES ('Charlie Brown');
INSERT INTO `count_me_up`.`voters` (`name`) VALUES ('Noah Wilson');
INSERT INTO `count_me_up`.`voters` (`name`) VALUES ('William Evans');
INSERT INTO `count_me_up`.`voters` (`name`) VALUES ('Oscar Thomas');
INSERT INTO `count_me_up`.`voters` (`name`) VALUES ('Oliver Johnson');


CREATE TABLE IF NOT EXISTS `votes` (
  `id` int(11) NOT NULL,
  `voter_id` int(11) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `voted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `votes`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
