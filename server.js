var express = require('express')
var mongoose = require('mongoose')
var bodyParser = require('body-parser')

var app = express()

app.get('/', function(req, res) {
  res.send('"Count Me Up" poll API')
})

// GET http://localhost:3000/polls
// returns all polls
app.get('/polls', require('./api/polls'))

// GET http://localhost:3000/candidates
// returns all candidates
app.get('/candidates', require('./api/candidates'))

// GET http://localhost:3000/voters
// returns all voters
app.get('/voters', require('./api/voters'))

// POST http://localhost:3000/:pollId/:candidateId/:voterId
// pollId INT
// candidateId INT
// voterId INT
// allows voter to vote candidate of poll
app.post('/vote/:pollId/:candidateId/:voterId', require('./api/vote'))

// GET http://localhost:3000/get_voters/:pollId
// pollId INT
// returns all the votes for the input poll
app.get('/get_votes/:pollId', require('./api/get_votes'))

// DELETE http://localhost:3000/clear_votes
// clears all the votes from the database (used for testing)
app.delete('/clear_votes', require('./api/clear_votes'))

app.listen(3000)
console.log('API Running on port 3000');

