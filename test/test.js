var should = require('should')
var request = require('request')
var expect = require('chai').expect
var baseUrl = 'http://localhost:3000/'
var util = require('util')

// polls tests
describe('poll tests', function() {
  it('poll exists', function(done) {
    request.get({url: baseUrl + 'polls'}, function(err, response, body) {
        expect(response.statusCode).to.equal(200);
        done();
    })
  })
})

// candidates tests
describe('candidates tests', function() {
  it('candidates exist', function(done) {
    request.get({url: baseUrl + 'candidates'}, function(err, response, body) {
        expect(response.statusCode).to.equal(200);
        done();
    })
  })
})

// voters tests
describe('voters tests', function() {
  it('voters exist', function(done) {
    request.get({url: baseUrl + 'voters'}, function(err, response, body) {
        expect(response.statusCode).to.equal(200);
        done();
    })
  })
})

// clear votes tests
describe('clear votes tests', function() {
  it('delete existing votes', function(done) {
    request.delete({url: baseUrl + 'clear_votes'}, function(err, response, body) {
        expect(response.statusCode).to.equal(200);
        done();
    })
  })
})

// vote tests
describe('votes tests', function() {
  it('invalid / non-existing pollId', function(done) {
    request.post({url: baseUrl + 'vote/aqqqq/1/1'}, function(err, response, body) {
        expect(response.statusCode).to.equal(400);
        done();
    })
  })
  it('invalid / non-existing candidateId', function(done) {
    request.post({url: baseUrl + 'vote/1/aa/1'}, function(err, response, body) {
        expect(response.statusCode).to.equal(400);
        done();
    })
  })
  it('invalid / non-existing voterId', function(done) {
    request.post({url: baseUrl + 'vote/1/1/aa'}, function(err, response, body) {
        expect(response.statusCode).to.equal(400);
        done();
    })
  })
  it('voter 5 can vote candidate A', function(done) {
    request.post({url: baseUrl + 'vote/1/1/5'}, function(err, response, body) {
        expect(response.statusCode).to.equal(201);
        done();
    })
  })
})

// get votes tests
describe('get votes tests', function() {
  it('get all votes for poll "count me up"', function(done) {
    request.get({url: baseUrl + 'get_votes/1'}, function(err, response, body) {
        expect(response.statusCode).to.equal(200);
        done();
    })
  })
})
